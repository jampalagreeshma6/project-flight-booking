package com.ts;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.AirportDao;
import com.dao.BookDao;
import com.dao.FlightDao;
import com.dao.UserDao;
import com.model.Airport;
import com.model.Book;

import com.model.Flight;
import com.model.User; 

@CrossOrigin(origins="*")
@RestController
public class UserController {
    @Autowired
    private UserDao userDao;
    @Autowired
    private BookDao bookDao;
    @Autowired
    private AirportDao airportDao;
    @Autowired
    private FlightDao flightDao;

    
    //register paths
    @PostMapping("register")
    public void register(@RequestBody User user){ 
        System.out.println("data received:" + user);
        userDao.register(user);
    }
    @RequestMapping("showAllUsers")
	public List<User> showAllUsers(){
		List<User> userList = new ArrayList<User>();
		 return userDao.getAllusers();
    }
    
    @DeleteMapping("/deletebyid/{id}")
    public User deleteUserById(@PathVariable("id")int id){
    	System.out.println("record deleted "+id);
    	return userDao.deleteUserById(id);
    }
   	
    //booking paths
    @PostMapping("registerbook")
    public void registerBook(@RequestBody Book book){ 
        System.out.println("data received:" + book);
        bookDao.registerBook(book);
    }
    @RequestMapping("showAllBookings")
   	public List<Book> showAllBookings(){
   		List<Book> BookList = new ArrayList<Book>();
   		 return bookDao.getAllBookings();
       }
    
    @GetMapping("/login/{emailId}/{password}")
	public User login(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
	    return userDao.login(emailId, password);
	}
    
   
    
    
    // airports paths
    @PostMapping("registerairport")
    public void registerAirport(@RequestBody Airport airport){ 
        System.out.println("data received:" + airport);
        airportDao.registerairport(airport);
    }
    
    @RequestMapping("showAllAirports")
   	public List<Airport> showAllAirports(){
   		List<Airport> AirportList = new ArrayList<Airport>();
   		 return airportDao.getAllAirports();
       }
    
    //flight paths
    @PostMapping("registerflight")
    public void registerFlight(@RequestBody Flight flight){ 
        System.out.println("data received:" + flight);
        flightDao.registerflight(flight);
    }
    
   
    @RequestMapping("showAllFlights")
   	public List<Flight> showAllFlights(){
   		List<Flight> FlightList = new ArrayList<Flight>();
   		 return flightDao.getAllFlights();
       }
    
    
    
    @RequestMapping("getTicketName/{name}")
	public Book getEmployeeByName(@PathVariable("name") String name){
      System.out.println("data retrieved"+name);
		return bookDao.getEmpByName(name);
    }
    
    
    @PutMapping("updateBooking/{id}")
	public Book updateBooking( @PathVariable int id ,@RequestBody Book book) {
	  book.setId(id);
	  return bookDao.updateBooking(book);
	}
    
    @DeleteMapping("/deleteBookingById/{id}")
    public void deleteBookById(@PathVariable("id") int id) {
        bookDao.deleteBookingById(id);
        System.out.println("Record deleted with ID: " + id);
    }

    
}
