package com.dao;


import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.Book;
import com.model.User;



@Repository
  public interface BookRepo extends JpaRepository<Book, Integer> {
	@Query("SELECT e FROM Book e WHERE e.name =:name")
	Book findByName(@Param("name")String name);
	
	 User deleteById(int id);
	
	 
	 Book save(Book book);

   }


