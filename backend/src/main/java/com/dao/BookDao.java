package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Book;



	

	@Service
	public class BookDao {

		@Autowired
		private BookRepo bookRepo;
		public void registerBook(Book book){
			bookRepo.save(book);
		}
		public List<Book> getAllBookings(){
			return bookRepo.findAll();
		}
		
		public Optional<Book> getEmpById(int id) {
			return bookRepo.findById(id);
		}
		

//		public List<Book> getEmpByEmail(String email){
//			return bookRepo.getEmpByEmail(email);
//		}
		
		public Book getEmpByName(String name) {
			return bookRepo.findByName(name);
		}
		
		
		
		public void deleteBookingById(int id) {
	        bookRepo.deleteById(id);
	    }
		
		public Book updateBooking( Book book) {  
			return bookRepo.save(book);
		}
		
	}


