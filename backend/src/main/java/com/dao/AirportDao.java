package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Book;
import com.model.Airport;

@Service
public class AirportDao {
	@Autowired
	private AirportRepo airportRepo;
	public void registerairport(Airport airport){
		airportRepo.save(airport);
	}
	
	public List<Airport> getAllAirports(){
		return airportRepo.findAll();
	}
}
