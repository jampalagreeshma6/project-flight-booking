package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Book;
import com.model.Flight;


@Service	
  public class FlightDao {
		@Autowired
		private FlightRepo flightRepo;
		public void registerflight(Flight flight){
			flightRepo.save(flight);
		}
		
		public List<Flight> getAllFlights(){
			return flightRepo.findAll();
		}
		
		
}
