package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.model.User;

@Service
public class UserDao {

	@Autowired
	private UserRepo userRepo;
	public void register(User user){
		userRepo.save(user);
	}
	public List<User> getAllusers(){
		return userRepo.findAll();
	}
	
	public Optional<User> getEmpById(int id) {
		return userRepo.findById(id);
	}
	
//	public User getEmpByName(String name) {
//		return userRepo.findByName(name);
//	}
	public List<User> getEmpByEmail(String email){
		return userRepo.getEmpByEmail(email);
	}
	
	public User login(String emailId, String password) {
	    return userRepo.login(emailId, password);
	}
	public User deleteUserById(int id) {
		// TODO Auto-generated method stub
		return   userRepo.deleteById(id);
	}
}
