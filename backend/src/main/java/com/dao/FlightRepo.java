package com.dao;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.model.Flight;

@Repository
  public interface FlightRepo extends JpaRepository<Flight, Integer> {
	

   }