package com.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.model.User;


@Repository
  public interface UserRepo extends JpaRepository<User, Integer> {
	
//	@Query("SELECT e FROM Employee e WHERE e.name =:name")
//	User findByName(@Param("name")String name);
	
	List<User> getEmpByEmail(String email);
	
	@Query("from User e where e.email = :emailId and e.password = :password")
	public User login(@Param("emailId") String emailId, @Param("password") String password);
	
	  User deleteById(int id);

   }
