package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Book {
  @Id
@GeneratedValue
   private int id;
   private String name;
   private String origin;
   private String destination;
   private String departure;
   private String returndate;
   
   private int adults;
   private int children;
   private int infant;
  
   
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getOrigin() {
	return origin;
}

public void setOrigin(String origin) {
	this.origin = origin;
}
public String getDestination() {
	return destination;
}
public void setDestination(String destination) {
	this.destination = destination;
}
public String getDeparture() {
	return departure;
}
public void setDeparture(String departure) {
	this.departure = departure;
}
public String getReturndate() {
	return returndate;
}
public void setReturndate(String returndate) {
	this.returndate = returndate;
}
public int getAdults() {
	return adults;
}
public void setAdults(int adults) {
	this.adults = adults;
}
public int getChildren() {
	return children;
}
public void setChildren(int children) {
	this.children = children;
}
public int getInfant() {
	return infant;
}
public void setInfant(int infant) {
	this.infant = infant;
}


   
   

   
}
