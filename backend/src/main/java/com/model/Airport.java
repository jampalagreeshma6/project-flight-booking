package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Airport {
	@Id
	@GeneratedValue
   private int id;
   private int airportcode;
   private String airportname;
   private String airportlocation;
  
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getAirportcode() {
	return airportcode;
}
public void setAirportcode(int airportcode) {
	this.airportcode = airportcode;
}
public String getAirportname() {
	return airportname;
}
public void setAirportname(String airportname) {
	this.airportname = airportname;
}
public String getAirportlocation() {
	return airportlocation;
}
public void setAirportlocation(String airportlocation) {
	this.airportlocation = airportlocation;
}
@Override
public String toString() {
	return "Airport [id=" + id + ", airportcode=" + airportcode + ", airportname=" + airportname
			+ ", airportlocation=" + airportlocation + "]";
}
  
}
